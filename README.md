# Université de La Rochelle
## Master 2 - UE Web 3.0
### Projet Ontologie : Mise en place d'une ontologie sur les Pokémons

### Technologie
- XML/OWL

### Auteurs
- Antoine Orgerit
- Lisa Fougeron
- François Gréau